import matplotlib.pyplot as plt
from math import pow, sqrt, floor, ceil


class FCMCluster:
    def __init__(self, points, number_clusters, sigma, m):
        self.points = points
        self.number_clusters = number_clusters
        self.number_data = len(points)
        self.number_dimensions = len(points[0])
        self.u = [[0 for x in range(self.number_clusters)] for y in range(self.number_data)]
        self._initialize_u_linear_distribute_data()
        self.u_old = [[0 for x in range(self.number_clusters)] for y in range(self.number_data)]
        self.c = [{} for i in range(self.number_clusters)]

        self.sigma = sigma
        self.m = m

    def _initialize_u_linear_distribute_uniform(self):
        left_x = +1
        right_x = -1
        for i in range(self.number_data):
            if self.points[i][0] < left_x:
                left_x = self.points[i][0]
            if self.points[i][0] > right_x:
                right_x = self.points[i][0]

        for i in range(self.number_data):
            for j in range(self.number_clusters):
                scale = (right_x - left_x) / (self.number_clusters - 1)
                if abs(self.points[i][0] - (j * scale + left_x)) <= scale:
                    self.u[i][j] = 1 - abs(self.points[i][0] - (j * scale + left_x)) / scale
                else:
                    self.u[i][j] = 0

    def _initialize_u_linear_distribute_data(self):
        if self.number_clusters == 1:
            for i in range(self.number_data):
                self.u[i][0] = 1
            return

        data_x = []
        for i in range(self.number_data):
            data_x.append(self.points[i][0])
        data_x.sort()
        scale = self.number_data / (self.number_clusters - 1)
        tmp = []
        for i in range(self.number_clusters - 1):
            tmp.append(data_x[int(scale * i)])
        tmp.append(data_x[-1])

        for i in range(self.number_data):
            for j in range(self.number_clusters):
                px = self.points[i][0]
                if px == tmp[j]:
                    self.u[i][j] = 1
                elif px > tmp[j]:
                    if px < tmp[j + 1]:
                        self.u[i][j] = 1 - (px - tmp[j]) / (tmp[j + 1] - tmp[j])
                    else:
                        self.u[i][j] = 0
                else:
                    if px < tmp[j - 1]:
                        self.u[i][j] = 1 - (tmp[j] - px) / (tmp[j] - tmp[j - 1])
                    else:
                        self.u[i][j] = 0

    def solve(self):
        while True:
            self._calculate_c()
            self._update_u()
            if self._condition_terminate():
                break

    def _copy_u(self):
        for i in range(self.number_data):
            for j in range(self.number_clusters):
                self.u_old[i][j] = self.u[i][j]

    def _calculate_c(self):
        for j in range(self.number_clusters):
            denominator = 0
            for i in range(self.number_data):
                denominator += pow(self.u[i][j], self.m)

            for d in range(self.number_dimensions):
                numerator = 0
                for i in range(self.number_data):
                    numerator += pow(self.u[i][j], self.m) * self.points[i][d]
                self.c[j][d] = numerator / denominator

    def _update_u(self):
        self._copy_u()
        for i in range(self.number_data):
            for j in range(self.number_clusters):
                self.u[i][j] = self._calculate_u(self.points[i], self.c[j])

    def _calculate_u(self, point_data, point_c):
        denominator = 0
        for k in range(self.number_clusters):
            if self._distance(point_data, self.c[k]) == 0:
                if self._distance(point_data, point_c) == 0:
                    denominator += 1
                else:
                    denominator += pow(1000 * 1000 * 1000, 2.0 / (self.m - 1))
            else:
                denominator += pow(
                    self._distance(point_data, point_c) / self._distance(point_data, self.c[k])
                    , 2.0 / (self.m - 1)
                )
        return 1 / denominator

    def plot(self):
        if self.number_dimensions != 2:
            return
        list_x = []
        list_y = []
        for point in self.points:
            x = point[0]
            y = point[1]
            list_x.append(x)
            list_y.append(y)

        plt.scatter(list_x, list_y, color='blue')

        list_c_x = []
        list_c_y = []
        for i in range(self.number_clusters):
            list_c_x.append(self.c[i][0])
            list_c_y.append(self.c[i][1])

        plt.scatter(list_c_x, list_c_y, color='red')
        plt.show()

    def plot_boundary(self):
        if self.number_dimensions != 2:
            return
        x_left = +1000
        x_right = -1000
        y_up = -1000
        y_down = +1000

        for i in range(self.number_data):
            if self.points[i][0] < x_left:
                x_left = self.points[i][0]
            if self.points[i][0] > x_right:
                x_right = self.points[i][0]

            if self.points[i][1] < y_down:
                y_down = self.points[i][1]
            if self.points[i][1] > y_up:
                y_up = self.points[i][1]

        famous_color = ['red', 'blue', 'green', 'yellow','orange',
                        'purple','brown','gray','cyan']
        x_list = []
        y_list = []
        color_list = []
        for i in range(floor(x_left * 200), ceil(x_right * 200)):
            for j in range(floor(x_left * 200), ceil(x_right * 200)):
                x_list.append(i / 200)
                y_list.append(j / 200)
                u_tmp = []
                for k in range(self.number_clusters):
                    point = dict()
                    point[0] = i / 200
                    point[1] = j / 200
                    u_tmp.append(self._calculate_u(point, self.c[k]))
                maxim_u = -1
                maxim_arg = -1
                for k in range(self.number_clusters):
                    if u_tmp[k] > maxim_u:
                        maxim_u = u_tmp[k]
                        maxim_arg = k
                color_list.append(famous_color[maxim_arg])

        for i in range(len(x_list)):
            plt.scatter(x_list[i], y_list[i], color=color_list[i])

        plt.show()

    @staticmethod
    def _distance(point1, point2):
        sum = 0.0
        number_dimensions = len(point1)
        for d in range(number_dimensions):
            sum += pow(point1[d] - point2[d], 2)
        return sqrt(sum)

    def _condition_terminate(self):
        max_distance = 0
        for i in range(self.number_data):
            for j in range(self.number_clusters):
                max_distance = max(max_distance, abs(self.u_old[i][j] - self.u[i][j]))
        return max_distance < self.sigma

    def calculate_cost(self):
        cost = 0
        for i in range(self.number_data):
            for j in range(self.number_clusters):
                cost += pow(self.u[i][j], self.m) * pow(self._distance(self.points[i], self.c[j]), 2)

        return cost
