import csv


class FileInterface:
    def __init__(self, file_address):
        self.file_address = file_address

    def read_points(self):
        # read csv file as a list of lists
        with open(self.file_address, 'r') as read_obj:
            # pass the file object to reader() to get the reader object
            csv_reader = csv.reader(read_obj)
            # Pass reader object to list() to get a list of lists
            list_of_rows = list(csv_reader)
            data_list = list_of_rows[1:]
            points = []
            for row in data_list:
                point = []
                for index in row:
                    point.append(float(index))
                points.append(point)
            return points
