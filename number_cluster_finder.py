from fcm_cluster import FCMCluster
import matplotlib.pyplot as plt


class NumberClusterFinder:
    def __init__(self, points, m, sigma):
        self.points = points
        self.m = m
        self.sigma = sigma
        self.max_number = 10

    def elbow_finder(self):
        costs = []
        indexes = []
        for i in range(1, self.max_number+1):
            fcmCluster = FCMCluster(points=self.points,
                                    sigma=self.sigma,
                                    m=self.m, number_clusters=i)
            fcmCluster.solve()
            costs.append(fcmCluster.calculate_cost() * i)
            indexes.append(i)
        plt.scatter(indexes, costs, color='blue')
        plt.show()
        # for i in range(1,len(costs)):
        #     print()
        #     if costs[i-1]-costs[i] < costs[i] - costs[i+1]:
        #         return i
        for i in range(len(costs)):
            if costs[i] < costs[i + 1]:
                return i+1
