from file_interface import FileInterface
from fcm_cluster import FCMCluster
from number_cluster_finder import NumberClusterFinder

SIGMA = 0.01
M = 2

fileInterface = FileInterface("./data/sample4.csv")
points = fileInterface.read_points()

numberClusterFinder = NumberClusterFinder(points=points, m=M, sigma=SIGMA)
number_cluster = numberClusterFinder.elbow_finder()

fcmCluster = FCMCluster(points=points, number_clusters=number_cluster, sigma=SIGMA, m=M)
fcmCluster.solve()
fcmCluster.plot()
print("Number Clusters: ", number_cluster)
print("Cost: ", fcmCluster.calculate_cost())
fcmCluster.plot_boundary()
